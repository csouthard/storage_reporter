# Storage Reporter

A small ruby script to extract basic project information along with storage statistics from the GitLab API.

## Rationale

There are some limitations to the Usage Quota projects table like sorting and filtering. This project provides a small tool to generate a report of usage data across all projects.

## Usage

### Requirements

- Ruby 2.7+, ideally 3+ ([how to setup a ruby environment](https://www.jetbrains.com/help/ruby/set-up-a-ruby-development-environment.html); consider [asdf](https://asdf-vm.com/guide/getting-started.html) for Ruby version management; )
- A GitLab [Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) with API permissions

### Setup

- clone this project
- `cd /path/to/storage_reporter`
- `bundle install`
- `cp .env.example .env`
- update `.env` with your `GITLAB_PERSONAL_TOKEN`

### Run

- `./bin/project_storage`
- if successful, a `projects.csv` will be saved in this directory

## Contributions

Please see [CONTRIBUTING.md](./CONTRIBUTING.md)

## License

Pending review from GitLab Legal Department


