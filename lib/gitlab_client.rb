require "bundler/setup"
require "dotenv/load"
require "httparty"
require "addressable"
require "erb"
require "tty-spinner"

module GitlabClient
  include ERB::Util
  include HTTParty
  include Addressable
  require "addressable/template"

  Dotenv.require_keys("GITLAB_PERSONAL_TOKEN")

  PRIVATE_TOKEN = ENV.fetch("GITLAB_PERSONAL_TOKEN")
  HEADERS = {"Private-Token": PRIVATE_TOKEN}
  BASE_URL = "https://gitlab.com/api/v4"

  ### GRAPHQL ###

  # arguments: optionally takes a membership parameter which is true by default and a cursor for pagination
  # accounting for errors:
  # {"errors"=>[{"message"=>"Variable $after is declared by anonymous query but not used", "locations"=>[{"line"=>1, "column"=>7}], "path"=>["query"], "extensions"=>{"code"=>"variableNotUsed", "variableName"=>"after"}}]}
  # return a parsed response that should be a hash with a project object containing an edges list that contains individual project nodes (see query below)
  #   pagination will be in a `pageInfo` object with a value and cursor
  #   response.parsed_response.dig("data", "projects", "pageInfo", "hasNextPage")
  def projects_with_stats(membership: true)
    spinners = TTY::Spinner::Multi.new
    project_nodes = []

    # NOTE(csouthard): default values for the first loop which has a different shape. Overridden if we need to loop.
    variables = {"membership" => membership}
    query_args = "$membership:Boolean"
    projects_args = "membership:$membership"

    loop do
      projects_query = <<-HEREDOC
        query(#{query_args}) {
          projects(#{projects_args}) {
            edges {
              node {
                id
                fullPath
                nameWithNamespace
                webUrl
                statistics {
                  buildArtifactsSize
                  commitCount
                  containerRegistrySize
                  lfsObjectsSize
                  packagesSize
                  pipelineArtifactsSize
                  repositorySize
                  snippetsSize
                  storageSize
                  uploadsSize
                  wikiSize
                }
                repositorySizeExcess
              }
            }
            pageInfo {
              endCursor
              startCursor
              hasNextPage
            }
          }
        }
      HEREDOC

      spinner = spinners.register("[:spinner] query variables: #{variables}")
      spinner.auto_spin

      results = HTTParty.post(
        "https://gitlab.com/api/graphql",
        headers: {
          Authorization: "Bearer #{PRIVATE_TOKEN}",
          "Content-Type": "application/json"
        },
        body: {"query" => projects_query, "variables" => variables}.to_json
      )

      if results.parsed_response.key?("errors")
        puts "GitLab returned an error: #{results.parsed_response["errors"]}"
        spinner.error("error")
        raise StopIteration
      end

      project_nodes << results.parsed_response.dig("data", "projects", "edges")

      if results.parsed_response.dig("data", "projects", "pageInfo", "hasNextPage")
        end_cursor = results.parsed_response.dig("data", "projects", "pageInfo", "endCursor")
        variables = {"membership" => membership, "after" => end_cursor}
        query_args = "$membership:Boolean, $after:String"
        projects_args = "membership:$membership, after:$after"
      else
        spinner.success("success; stopping")
        raise StopIteration
      end
      spinner.success("success")
    end
    project_nodes.flatten
  end

  def project_container_registry_stats(path_with_namespace)
    project_query = <<-HEREDOC
      query($fullPath: ID!) {
        project(fullPath: $fullPath) {
          statistics {
            containerRegistrySize
          }
        }
      }
    HEREDOC

    response = HTTParty.post(
      "https://gitlab.com/api/graphql",
      headers: {
        Authorization: "Bearer #{PRIVATE_TOKEN}",
        "Content-Type": "application/json"
      },
      body: {"query" => project_query, "variables" => {"fullPath" => path_with_namespace}}.to_json
    )

    if response.parsed_response["errors"]
      puts "GitLab returned an error: #{response.parsed_response["errors"]}"
      return "returned error"
    end

    response.parsed_response.dig("data", "project", "statistics", "containerRegistrySize") || "-"
  end

  ### REST ###
  def fetch_paginated_data(path, param)
    Enumerator.new do |yielder|
      page = 1

      loop do
        results = get(path, page, param)

        if results.success? && !JSON.parse(results.body).empty?
          results.map { |item| yielder << item }
          page += 1
        else
          raise StopIteration
        end
      end
    end.lazy
  end

  private

  def get(path, page = 1, query_params = {})
    query_options = base_options.merge(page: page).merge(query_params)
    HTTParty.get(full_path(path, query_options),
      follow_redirects: true,
      maintain_method_across_redirects: true,
      headers: HEADERS)
  end

  def full_path(path, query_options = base_options)
    template = Addressable::Template.new("#{BASE_URL}/#{path}{?query*}")
    template.expand({"query" => query_options})
  end

  def base_options
    {per_page: 20, page: 1}
  end
end
