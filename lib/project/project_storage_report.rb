require "bundler/setup"
require "csv"
require "date"

require_relative "../gitlab_client"

class ProjectStorageReport
  include GitlabClient
  extend GitlabClient
  extend Gem::Deprecate

  attr_accessor :path, :params
  def initialize(path = "projects", params = {statistics: true, owned: true})
    @path = path
    @params = params
  end

  Project = Struct.new(:id, :path_with_namespace, :name_with_namespace, :web_url, :statistics, :repository_size_excess)

  def projects
    project_data_via_graphql
  end
  deprecate :projects, :project_data_via_graphql, 2022, 12

  def project_data_via_graphql
    puts "Attempting to gather project statistic data"
    projects_with_stats(membership: true).map { |project|
      Project.new(project["node"]["id"], project["node"]["fullPath"], project["node"]["nameWithNamespace"], project["node"]["webUrl"], project["node"]["statistics"], project["node"]["repositorySizeExcess"])
    }
  end

  def self.to_csv
    project_list = new.project_data_via_graphql
    headers = ["id", "fullPath", "nameWithNamespace", "webUrl", "repositorySizeExcess"].push(project_list.first.statistics.keys).flatten
    File.open(file_name, "w") do |f|
      f.write(CSV.generate_line(headers))

      project_list.each do |project|
        puts "#{project.name_with_namespace} - #{project.statistics.inspect}"
        stats = project&.statistics.nil? ? project_list.first.statistics.keys.collect { |k| nil } : project.statistics.values

        row = [project.id, project.path_with_namespace, project.name_with_namespace, project.web_url, project.repository_size_excess, stats].flatten
        f.write(CSV.generate_line(row))
      end
    end
    puts "Check projects.csv in this directory"
  end

  private

  def file_name
    today = Date.today.iso8601
    "projects-#{today}.csv"
  end
end
